#
# cpufrequtils for Android
#

LOCAL_PATH := $(call my-dir)

# Define vars for library that will be build statically.
include $(CLEAR_VARS)
LOCAL_MODULE := libcpufreq
LOCAL_C_INCLUDES := $(LOCAL_PATH)/lib
LOCAL_SRC_FILES :=  lib/cpufreq.c \
                    lib/sysfs.c

include $(BUILD_STATIC_LIBRARY)


# cpufreq-info binary
include $(CLEAR_VARS)
LOCAL_MODULE := cpufreq-info
LOCAL_C_INCLUDES := $(LOCAL_PATH)/lib \
                    $(LOCAL_PATH)/utils
LOCAL_SRC_FILES :=  utils/info.c 

# Optional compiler flags.
LOCAL_CFLAGS   = -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare \
                 -DPACKAGE=\"cpufrequtils\" -DPACKAGE_BUGREPORT=\"cpufreq@vger.kernel.org\" -DVERSION=\"008\"
LOCAL_STATIC_LIBRARIES := libc libcpufreq
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
LOCAL_MODULE_TAGS := eng debug
LOCAL_FORCE_STATIC_EXECUTABLE := true

include $(BUILD_EXECUTABLE)


# cpufreq-set binary
include $(CLEAR_VARS)
LOCAL_MODULE := cpufreq-set
LOCAL_C_INCLUDES := $(LOCAL_PATH)/lib \
                    $(LOCAL_PATH)/utils
LOCAL_SRC_FILES :=  utils/set.c 

# Optional compiler flags.
LOCAL_CFLAGS   = -Wall -Wchar-subscripts -Wpointer-arith -Wsign-compare \
                 -DPACKAGE=\"cpufrequtils\" -DPACKAGE_BUGREPORT=\"cpufreq@vger.kernel.org\" -DVERSION=\"008\"
LOCAL_STATIC_LIBRARIES := libc libcpufreq
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
LOCAL_MODULE_TAGS := eng debug
LOCAL_FORCE_STATIC_EXECUTABLE := true

include $(BUILD_EXECUTABLE)

